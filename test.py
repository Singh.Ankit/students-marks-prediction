import joblib
model= joblib.load('student_marks_prediction.pkl')
n= int(input("Study hours: "))
print("Predicted Marks: ", model.predict([[n]])[0][0].round(2))